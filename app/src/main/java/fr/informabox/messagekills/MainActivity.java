package fr.informabox.messagekills;
import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.app.Activity;

import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.telephony.SmsManager;

import android.view.View;

import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;

public class MainActivity extends Activity {
    Button buttonSend;
    EditText textPhoneNo;
    EditText textSMS;
    Button cnt;
    Button jure;
    Spinner s1;
    Spinner s2;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        buttonSend = (Button) findViewById(R.id.btnSendSMS);
        cnt = (Button) findViewById(R.id.btncnt);
        jure = (Button) findViewById(R.id.btnjure);
        textPhoneNo = (EditText) findViewById(R.id.editText);
        textSMS = (EditText) findViewById(R.id.editText2);

        s1 = (Spinner) findViewById(R.id.spinfois);
        s2 = (Spinner) findViewById(R.id.spindelay);

        buttonSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                int fois = Integer.valueOf(s1.getSelectedItem().toString());
                for (int ii=0; ii<fois; ii++) {
                    int secondes = Integer.valueOf(s2.getSelectedItem().toString());
                    try {
                        SmsManager smgr = SmsManager.getDefault();
                        smgr.sendTextMessage(textPhoneNo.getText().toString(), null, textSMS.getText().toString(), null, null);
                        Toast.makeText(MainActivity.this, "SMS Sent Successfully", Toast.LENGTH_SHORT).show();
                    } catch (Exception e) {
                        Toast.makeText(MainActivity.this, "SMS Failed to Send, Please try again", Toast.LENGTH_SHORT).show();
                    }
                    try {
                        TimeUnit.SECONDS.sleep(secondes);
                    } catch (Exception e) {
                        Toast.makeText(MainActivity.this, "WAIT Failed ! ", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });




        cnt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                int fois = Integer.valueOf(s1.getSelectedItem().toString());
                for (int ii=0; ii<fois; ii++) {
                    int secondes = Integer.valueOf(s2.getSelectedItem().toString());
                    try {
                        SmsManager smgr = SmsManager.getDefault();
                        smgr.sendTextMessage(textPhoneNo.getText().toString(), null, String.valueOf(fois-ii)+"\n"+textSMS.getText().toString(), null, null);
                        Toast.makeText(MainActivity.this, "SMS Sent Successfully", Toast.LENGTH_SHORT).show();
                    } catch (Exception e) {
                        Toast.makeText(MainActivity.this, "SMS Failed to Send, Please try again", Toast.LENGTH_SHORT).show();
                    }
                    try {
                        TimeUnit.SECONDS.sleep(secondes);
                    } catch (Exception e) {
                        Toast.makeText(MainActivity.this, "WAIT Failed ! ", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });





        jure.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String randomElement;
                List<String> Lines = Arrays.asList(getResources().getStringArray(R.array.injures));
                Random rand = new Random();
                int fois = Integer.valueOf(s1.getSelectedItem().toString());
                for (int ii=0; ii<fois; ii++) {
                    randomElement = Lines.get(rand.nextInt(Lines.size()));
                    int secondes = Integer.valueOf(s2.getSelectedItem().toString());
                    try {
                        SmsManager smgr = SmsManager.getDefault();
                        smgr.sendTextMessage(textPhoneNo.getText().toString(), null, randomElement+"\n"+textSMS.getText().toString(), null, null);
                        Toast.makeText(MainActivity.this, "SMS Sent Successfully", Toast.LENGTH_SHORT).show();
                    } catch (Exception e) {
                        Toast.makeText(MainActivity.this, "SMS Failed to Send, Please try again", Toast.LENGTH_SHORT).show();
                    }
                    try {
                        TimeUnit.SECONDS.sleep(secondes);
                    } catch (Exception e) {
                        Toast.makeText(MainActivity.this, "WAIT Failed ! ", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
    }
}
